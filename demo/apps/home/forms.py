from django import forms
from django.contrib.auth.models import User
class ContactForm(forms.Form):
	Email = forms.EmailField(widget=forms.TextInput())
	Titulo = forms.CharField(widget=forms.TextInput())
	Texto = forms.CharField(widget=forms.Textarea())

class loginForm(forms.Form):
	username = forms.CharField(widget=forms.TextInput())
	password = forms.CharField(widget=forms.PasswordInput(render_value=False))

class registerForm(forms.Form):
	username     = forms.CharField(label='Nombre de Usuario',widget=forms.TextInput())
	email        = forms.EmailField(label='Correo',widget=forms.TextInput())
	password_one = forms.CharField(label='Password',widget=forms.PasswordInput(render_value=False))
	password_two = forms.CharField(label='Confirmar Password',widget=forms.PasswordInput(render_value=False))

	################### Validaciones ##########################
	def clean_username(self):
		username = self.cleaned_data["username"]
		try:
			u = User.objects.get(username=username)
		except User.DoesNotExist: # si no existe
			return username
		##si ya existe nos levanta una excepcion
		raise forms.ValidationError('Nombre de Usuario ya existe')

	def clean_email(self):
		email = self.cleaned_data["email"]
		try:
			u = User.objects.get(email=email)
		except User.DoesNotExist:
			return email
		raise forms.ValidationError('Email ya existe')

	def clean_password_two(self):
		password_one = self.cleaned_data["password_one"]
		password_two = self.cleaned_data["password_two"]
		if password_one == password_two:
			pass
		else:
			raise forms.ValidationError('Password no Coinciden')