from django.shortcuts import render_to_response
from django.template import RequestContext
from demo.apps.ventas.models import Producto
#from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponseRedirect,HttpResponse
from demo.apps.home.forms import ContactForm,loginForm,registerForm
##para el registro
from django.contrib.auth.models import User
from datetime import datetime
#Paginacion en Django
from django.core.paginator import Paginator,EmptyPage,InvalidPage
from demo.settings import URL_LOGIN
from django.contrib.auth.decorators import login_required
import django
import json


# Create your views here.

def index_view(request):
	return render_to_response("home/index.html",context_instance=RequestContext(request))

@login_required(login_url=URL_LOGIN)
def acerca_view(request):
	mensaje = "Esto es un msj desde mi vista"
	version = django.get_version()
	ctx ={
		'msj':mensaje,
		'version':version
	}
	return render_to_response("home/about.html",ctx)

def productos_view(request,pagina=1):
	if request.method == "POST":
		if "product_id" in request.POST:
			try:
				id_producto = request.POST["product_id"]
				print " id Producto : %s"%id_producto
				print json
				p = Producto.objects.get(pk=id_producto)
				print " id Producto BD : %s"%p.id
				mensaje = {"status":True,"product_id":p.id}
				p.delete() #eliminamos el objeto de la base de datos
				return HttpResponse(json.dumps(mensaje),mimetype="application/json")
			except:
				mensaje = {"status":False}
				return HttpResponse(json.dumps(mensaje),mimetype="application/json")
	lista_prod = Producto.objects.filter(status=True)
	paginator = Paginator(lista_prod,3) # cuantos productos quieres por pagina
	try:
		page = int(pagina)
	except:
		page = 1
	try:
		productos=paginator.page(page)
	except(EmptyPage,InvalidPage):
		productos = paginator.page(paginator.num_pages)
	ctx = {'productos':productos}
	return render_to_response("home/productos.html",ctx,context_instance=RequestContext(request))

def sigle_product_view(request,id_product):
	prod = Producto.objects.get(id=id_product)
	cats = prod.categorias.all() # obteniendo las categorias del producto encontrado
	ctx = {'producto':prod,'cats':cats}
	return render_to_response("home/SingleProduct.html",ctx,context_instance=RequestContext(request))

def contact_view(request):
	info_enviado = False
	email = ""
	titulo = ""
	texto = ""
	formulario = ContactForm()
	if request.method == "POST":
		formulario = ContactForm(request.POST)
		if formulario.is_valid():
			info_enviado = True
			email = formulario.cleaned_data["Email"]
			titulo = formulario.cleaned_data["Titulo"]
			texto = formulario.cleaned_data["Texto"]

			# configuracion para el envio del correo
			to_admin = 'janco1187@gmail.com'
			html_content = 'infromacion recibida  de  [%s]: <br/><b>***Mensaje**</b> <p>%s</p>'%(email,texto)
			msg = EmailMultiAlternatives('Correo de contacto ',html_content,'cunaviche@hotmail.com',[to_admin])
			msg.attach_alternative(html_content,'text/html') # definimos el contenido como HTML
			msg.send()
			#msg = send_mail('Correo de contacto ',html_content,'cunaviche@hotmail.com',[to_admin])

	ctx = {
			'form':formulario,
			'info_enviado':info_enviado,
			'email':email,
			'titulo':titulo,
			'texto':texto
		  }
	return render_to_response("home/contact.html",ctx,context_instance=RequestContext(request))

def login_view(request):
	mensaje = ""
	if request.user.is_authenticated():
		return HttpResponseRedirect('/')
	else:
		if request.method == 'POST':
			form = loginForm(request.POST)
			if form.is_valid():
				next = request.POST["next"]
				print "la url POST es:  %s"%next
				username = form.cleaned_data["username"]
				password = form.cleaned_data["password"]
				usuario = authenticate(username=username,password=password)
				if usuario is not None and usuario.is_active:
					login(request,usuario)
					return HttpResponseRedirect(next)
					#return HttpResponseRedirect('/')
				else:
					mensaje = "usuario y/o password incorrecto"
		next = request.REQUEST.get("next")
		print "la url es %s"%next
		form = loginForm()
		ctx = {'form' : form, 'mensaje':mensaje,'next':next}
		return render_to_response('home/login.html',ctx,context_instance=RequestContext(request))

def logout_view(request):
	logout(request)
	return HttpResponseRedirect('/')

def register_view(request):
	form = registerForm()
	if request.method=="POST":
		form = registerForm(request.POST)
		if form.is_valid():
			usuario = form.cleaned_data["username"]
			email = form.cleaned_data["email"]
			password_one = form.cleaned_data["password_one"]
			password_two = form.cleaned_data["password_two"]
			u = User.objects._create_user(username=usuario,email=email,password=password_one,is_staff=False,is_superuser=False)
			u.save()
			return render_to_response("home/thanks_register.html",context_instance=RequestContext(request))
		else:
			ctx = {
				'form':form
			}
			return render_to_response("home/register.html",ctx,context_instance=RequestContext(request))
	ctx = {
		'form':form
	}
	return render_to_response("home/register.html",ctx,context_instance=RequestContext(request))