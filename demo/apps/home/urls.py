from django.conf.urls import patterns, include, url

urlpatterns = patterns('demo.apps.home.views',
    # Examples:
     url(r'^$', 'index_view', name='vista_principal'),
     url(r'^about/$', 'acerca_view', name='vista_acerca'),
     url(r'^productos/$', 'productos_view', name='vista_productos'),
     url(r'^productos/page/(?P<pagina>.*)/$', 'productos_view', name='vista_productos'),
     url(r'^producto/(?P<id_product>.*)/$', 'sigle_product_view', name='vista_sigle_producto'),
     url(r'^contacto/$', 'contact_view', name='vista_contactp'),
     url(r'^login/$', 'login_view', name='vista_login'),
     url(r'^registro/$', 'register_view', name='vista_registro'),
     url(r'^logout/$', 'logout_view', name='vista_logout'),
    
)
