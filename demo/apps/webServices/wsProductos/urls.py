from django.conf.urls import patterns, include, url

urlpatterns = patterns('demo.apps.webServices.wsProductos.views',
    # Examples:
     url(r'^ws/productos/$', 'wsProductos_view', name='ws_productos_url'),
     url(r'^ws/productos/(?P<type>.*)/$', 'wsProductos_view', name='ws_productos_url'),
    
)
