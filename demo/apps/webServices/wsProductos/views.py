from django.http import HttpResponse
from demo.apps.ventas.models import Producto
# integramos la serializacion
from django.core import serializers


def wsProductos_view(request,type='json'):
	data = serializers.serialize(type,Producto.objects.filter(status=True))
	#retorna la informacion en formato json
	return HttpResponse(data,mimetype='application/'+type)