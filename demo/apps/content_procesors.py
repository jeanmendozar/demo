from demo.apps.home.models import UserProfile
import django

def user_image(request):
	try:
		imagen = None
		user = request.user
		up = UserProfile.objects.get(user=user)
		imagen = "/media/%s"%up.photo
	except:
		imagen ="/media/images/user.jpeg"
	return imagen

#contexto global para todas las templates
def my_procesors(request):
	context = {
				  "django_version":django.get_version(),
				  "get_imagen_profile":user_image(request),
			  }
	return context