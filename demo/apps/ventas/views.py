#encoding:utf8
from django.shortcuts import render_to_response
from django.template import RequestContext
from demo.apps.ventas.forms import addProductoForm
from demo.apps.ventas.models import Producto
from django.http import HttpResponseRedirect
def add_producto_view(request):
	info = 'iniciado'
	if request.method == "POST":
		form = addProductoForm(request.POST,request.FILES)
		if form.is_valid():
			add = form.save(commit=False)
			add.status=True
			add.save() # guardamos la informacion
			form.save_m2m() # guarda las relaciones manyTomany (categorias)
			info = 'Guardado satisfactoriamente'
			return HttpResponseRedirect('/producto/%s'%add.id)
	else:
		form=addProductoForm()
	ctx = {'form':form,'informacion':info}
	return render_to_response('ventas/addProducto.html',ctx,context_instance=RequestContext(request))		

"""
def add_producto_view(request):
	info = 'inicianizandolo'
	if request.user.is_authenticated():
		if request.method == 'POST':
			form = addProductoForm(request.POST,request.FILES)
			if form.is_valid():
				nombre =form.cleaned_data["nombre"]
				descripcion= form.cleaned_data["descripcion"]
				imagen = form.cleaned_data["imagen"] # este se entiende con el request.FILES
				precio = form.cleaned_data["precio"]
				stock = form.cleaned_data["stock"]

				p=Producto()
				if imagen: #generamos una pequeña validacion "si existe imagen"
					p.imagen=imagen
				p.nombre      = nombre
				p.descripcion = descripcion
				p.precio      = precio
				p.stock       = stock
				p.status      = True
				p.save() # guarda la informacion
				info = "se guardo satisfactoriamente !!!"
				form = addProductoForm()
			else:
				info = "informacion con datos incorrectos"
			ctx = {
					'form':form,
					'informacion':info
				}
			return render_to_response('ventas/addProducto.html',ctx,context_instance=RequestContext(request))
		else: #GET
			form = addProductoForm()
			ctx = {
				'form':form
			}
			return render_to_response('ventas/addProducto.html',ctx,context_instance=RequestContext(request))
	else:
		return HttpResponseRedirect('/');
"""

def edit_producto_view(request,id_product):
	info = 'iniciado'
	prod= Producto.objects.get(pk=id_product)
	if request.method == "POST":
		form = addProductoForm(request.POST,request.FILES)
		if form.is_valid():
			edit_prod = form.save(commit=False)
			edit_prod.status=True
			edit_prod.save() # guardamos el objeto
			form.save_m2m()
			return HttpResponseRedirect('/producto/%s'%edit_prod.id)
	else:
		form = addProductoForm(instance=prod)
	ctx = {
		'form':form,
		'producto':prod,
		'informacion':info
	}
	return render_to_response('ventas/editProducto.html',ctx,context_instance=RequestContext(request))
"""
def edit_producto_view(request,id_product=1):
	p = Producto.objects.get(pk=id_product)
	if request.method == 'POST':
		form = addProductoForm(request.POST,request.FILES)
		if form.is_valid():
			nombre      = form.cleaned_data["nombre"]
			descripcion = form.cleaned_data["descripcion"]
			precio      = form.cleaned_data["precio"]
			stock       = form.cleaned_data["stock"]
			imagen      = form.cleaned_data["imagen"]
			p.nombre = nombre
			p.precio = precio
			p.descripcion = descripcion
			p.stock = stock
			if imagen: # verificamos que la imagen sea correcto
				p.imagen = imagen
			p.save() # guardamos el modelo de manera  EDITAR
			return HttpResponseRedirect("/producto/%s"%p.id)
	if request.method == 'GET':
		form = addProductoForm(initial={
				'nombre':p.nombre,
				'descripcion':p.descripcion,
				'precio':p.precio,
				'stock':p.stock
			})
	ctx = {
		'form':form,
		'producto':p
	}
	return render_to_response('ventas/editProducto.html',ctx,context_instance=RequestContext(request))
"""