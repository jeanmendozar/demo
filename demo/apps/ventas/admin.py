from django.contrib import admin
from demo.apps.ventas.models import Cliente,Producto,categoriaProducto

##personalizando el admin
class ProductoAdmin(admin.ModelAdmin):
	list_display = ('nombre','thumbnail','precio','stock')
	list_filter = ('nombre','precio')
	search_fields = ['nombre','precio']
	fields = ('nombre','descripcion',('precio','stock','imagen'),'categorias','status')


# Register your models here.

admin.site.register(Cliente)
admin.site.register(Producto,ProductoAdmin)
admin.site.register(categoriaProducto)