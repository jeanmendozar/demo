from django.conf.urls import patterns, include, url

urlpatterns = patterns('demo.apps.ventas.views',
    # Examples:
     url(r'^add/Producto/$', 'add_producto_view', name='vista_agregar_producto'),
     url(r'^edit/Producto/(?P<id_product>.*)/$', 'edit_producto_view', name='vista_editar_producto'),
)
